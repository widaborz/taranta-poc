

# Taranta proof of concept

A Taranta proof-of-concepts basend on [Nx](https://nx.dev).

<p style="text-align: center;"><img src="https://taranta.readthedocs.io/en/develop/_static/img/taranta_logo.png" ></p>

## Project structure

The project includes: React, Jest for testing, Cypress for e2e tests

## Start the project

To start the project, run: 

`nx test taranta-poc`

and navigate to: http://localhost:4200/. The app will automatically reload if you change any of the source files.

There are also many [community plugins](https://nx.dev/community) you could add.

## Build

Run `nx build taranta-poc` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `nx test taranta-poc` to execute the unit tests via [Jest](https://jestjs.io).

Run `nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `nx e2e taranta-poc` to execute the end-to-end tests via [Cypress](https://www.cypress.io). (no test implemented yet)

Run `nx affected:e2e` to execute the end-to-end tests affected by a change.

## Understand your workspace

Run `nx graph` to see a diagram of the dependencies of your projects.

## VSStudio plugin

It is suggested to install Nx Console. Nx Console for Visual Studio Code. 